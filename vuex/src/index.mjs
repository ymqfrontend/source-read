import Vuex from '../dist/vuex.cjs.js'

const {
  version,
  Store,
  storeKey,
  createStore,
  install,
  useStore,
  mapState,
  mapMutations,
  mapGetters,
  mapActions,
  createNamespacedHelpers,
  createLogger
} = Vuex

console.log("================mjs================");

export {
  Vuex as default,
  version,
  Store,
  storeKey,
  createStore,
  install,
  useStore,
  mapState,
  mapMutations,
  mapGetters,
  mapActions,
  createNamespacedHelpers,
  createLogger
}
