/*
 * @Author       : ymq
 * @Date         : 2023-10-16 11:13:40
 * @LastEditors  : Do not edit
 * @LastEditTime : 2023-10-16 15:47:05
 * @Description  : Do not edit
 * @FilePath     : \vuex\examples\classic\counter\app.js
 */
import { createApp } from 'vue'
import Counter from './Counter.vue'
import store from './store'

const app = createApp(Counter)
app.use(store)

app.mount('#app')
